const Koa = require('koa');
const Router = require('koa-router');
const axios = require("axios");
const mongoose = require('mongoose');
const bodyParser = require('koa-bodyparser');

const app = new Koa();
const router = new Router();

//Environment Variables
require('dotenv').config()

//Connection to MongoDB
const db = mongoose.connection;
const host = process.env.DB_HOST;
const dbupdate = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};
mongoose.connect(host, dbupdate);

db.on('error', (err) => console.log('Error, DB Not Connected'));
db.on('connected', () => console.log('Connected To Mongo'));
db.on('disconnected', () => console.log('Mongo is Disconnected'));
db.on('open', () => console.log('Connection Made!'));

//Model Schema
const Movie = require('./model/movie.js')

router.get('/movie/:moviestr', async ctx => {
  var movieString = ctx.params.moviestr;
  var result;

  if (ctx.request.headers['year']) {
    var year = ctx.request.headers['year'];
    result = await axios.get(`https://www.omdbapi.com/?t=${movieString}&y=${year}&apikey=${process.env.API_KEY}`)

  } else {
    result = await axios.get(`https://www.omdbapi.com/?t=${movieString}&apikey=${process.env.API_KEY}`);
  }

  Movie.findOne({
    title: result.data.Title,
    year: result.data.Year,
    released: result.data.Released,
    director: result.data.Director,
    actors: result.data.Actors
  }, async function(err,res){
    if(err) console.log(err);
    if (res) {
      //result.data = "This has already been saved"
      ctx.response.body = "This has already been saved";
      console.log("This has already been saved");
    } else {
      const mov = await Movie.create({
        title: result.data.Title,
        year: result.data.Year,
        released: result.data.Released,
        genre: result.data.Genre,
        director: result.data.Director,
        actors: result.data.Actors,
        plot: result.data.Plot,
        ratings: []
      })

      result.data.Ratings.forEach( rating => {
        mov.ratings.push({
          source: rating.Source,
          value: rating.Value
        })
      });

      const updated = await mov.save()

      ctx.response.body = updated;
      console.log(result.data)
      console.log(updated)
      console.log(mov.ratings[0])
    }
  })
  //ctx.body = updated;
});

//Get all movies
router.get('/movie', async ctx => {
  //const all = await Movies.find().exec();
  var page;
  const limit = 5;
  if (ctx.request.headers['page']) {
    page = ctx.request.headers['page'];
  } else {
    page = 1;
  }

  try {
    const movies = await Movie.find()
                               .limit(limit * 1)
                               .skip((page - 1) * limit)
                               .exec();
    const count = await Movie.countDocuments();

    ctx.body = {
      movies,
      totalPages: Math.ceil(count / limit),
      currentPage: page
    };
  } catch (err) {
    console.error(err.message);
  }
});

router.post('/update', async ctx => {
  const findReplace = ctx.request.body;

  console.log(findReplace);

  const mov = await Movie.findOne({
    title: findReplace.movie
  });

  const plotString = mov.plot;

  const regex = new RegExp(findReplace.find, 'g');

  const stringUpdated = plotString.replace(regex, findReplace.replace);

  //const updated = await mov.save();

  ctx.body = stringUpdated;
  console.log(stringUpdated);
})

//Middleware
app.use(bodyParser());
app.use(router.routes());

app.use(async ctx => {
  ctx.body = 'Hello world';
});

app.listen(3000);
