const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const movieSchema = new Schema({
  title: String,
  year: String,
  released: String,
  genre: String,
  director: String,
  actors: String,
  plot: String,
  ratings: [{
    source: String,
    value: String
  }]
});

const Movie = mongoose.model('movies', movieSchema);

module.exports = Movie;
